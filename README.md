# Helm Chart for hetzner-cloud-controller-manager

This is a community Helm Chart for installing the hcloud-cloud-controller-manager in your Hetzner Cloud Kubernetes cluster.
The original sources of the hcloud-cloud-controller-manager can be found at
[https://github.com/hetznercloud/hcloud-cloud-controller-manager](https://github.com/hetznercloud/hcloud-cloud-controller-manager).

**Please note**: This project is a community project from a Hetzner customer, published for use by other Hetzner customers.
Neither the author nor this project is affiliated with Hetzner Online GmbH.


## Installation

### Add Helm Repository

```
helm repo add mlohr https://helm-charts.mlohr.com/
helm repo update
```

### Install to Kubernetes

In order to get hcloud-cloud-controller-manager running on your cluster,
you might need to consider some requirements regarding the network plugin configuration.
Take a look at [docs/networking.md](docs/networking.md) for more information.

After your Kubernetes cluster is installed, you can install hcloud-cloud-controller-manager:

  * Without using a private network:
    ```
    helm install -n kube-system hcloud-cloud-controller-manager mlohr/hcloud-cloud-controller-manager \
      --set manager.secret.create=true \
      --set manager.secret.hcloudApiToken=<HCLOUD API TOKEN>
    ```
  * With using a private network:
    ```
    helm install -n kube-system hcloud-cloud-controller-manager mlohr/hcloud-cloud-controller-manager \
      --set manager.secret.create=true \
      --set manager.secret.hcloudApiToken=<HCLOUD API TOKEN> \
      --set manager.privateNetwork.enabled=true \
      --set manager.privateNetwork.id=<HCLOUD NETWORK ID> \
      --set manager.privateNetwork.clusterSubnet=<HCLOUD NETWORK CIDR>
    ```

It is also possible to reuse an existing secret, e.g. from
[hcloud-csi-driver Helm Chart](https://gitlab.com/MatthiasLohr/hcloud-csi-driver-helm-chart):

  * Reuse existing secret:
    ```
    helm install -n kube-system hcloud-cloud-controller-manager mlohr/hcloud-cloud-controller-manager \
      --set manager.secret.name=<EXISTING SECRET NAME>
    ```

When Kubernetes is installed with `--cloud-provider=external`, coredns and your network plugins may not start,
since the nodes are not yet initialized by hcloud-cloud-controller-manager.
In order to make the Hetzner API endpoint reachable for hcloud-cloud-controller-manager,
you have to bring up coredns and your network.

You can bring up coredns by adding a toleration to run on uninitialized nodes:
```
kubectl -n kube-system patch deployment coredns --type json -p '[{"op":"add","path":"/spec/template/spec/tolerations/-","value":{"key":"node.cloudprovider.kubernetes.io/uninitialized","value":"true","effect":"NoSchedule"}}]'
```

In the next step, you have to set up your Kubernetes cluster networking.
[Read this file](https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/-/blob/master/docs/networking.md) for some information.

Depending on your network plugin, you also have to add tolerations there.
See [Network Plugins](https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/-/blob/master/docs/network-plugin-tolerations.md) for more information.


## Configuration Parameters

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `manager.addressFamily` | Cluster address family. Allowed values: IPv4, IPv6, DualStack | `IPv4` |
| `manager.privateNetwork.enabled` | Use private network for internal cluster communikcation | `false` |
| `manager.privateNetwork.id` | ID of the Hetzner private network (get with `hcloud network list`) | `nil` |
| `manager.privateNetwork.clusterSubnet` | Subnet used for internal server IPs | `10.233.255.0/24` |
| `manager.privateNetwork.disableAttachedCheck` | Disable the "master/server is attached to the network" check against the metadata service. | `false` |
| `manager.loadBalancers.enabled` | Enable Hetzner Load Balancer support | `true` |
| `manager.loadBalancers.defaults.disablePrivateIngress` | Disable private ingress for LoadBalancers by default | `false` |
| `manager.loadBalancers.defaults.usePrivateIp` | Use private IPs for LoadBalancers by default | `false` |
| `manager.loadBalancers.defaults.location` | Default LoadBalancer location (mutually exclusive with `.networkZone`) | `nil` |
| `manager.loadBalancers.defaults.networkZone` | Default LoadBalancer network zone (mutually exclusive with `.location`) | `nil` |
| `manager.deployment.image` | Image used for deploying the manager | `hetznercloud/hcloud-cloud-controller-manager:v1.11.1` |
| `manager.deployment.imagePullPolicy` | ImagePullPolicy | `IfNotPresent` |
| `manager.deployment.imagePullSecret` | ImagePullSecret | `nil` |
| `manager.deployment.debug` | Enable debugging for hcloud-cloud-controller-manager | `false` |
| `manager.deployment.labels` | Additional labels for Deployment resource | `{}` |
| `manager.deployment.annotations` | Additional annotations for Deployment resource | `{}` |
| `manager.deployment.template.labels` | Additional labels for Pod template | `{}` |
| `manager.deployment.template.annotations` | Additional annotations for Pod template | `{}` |
| `manager.secret.create` | Create a Secret for the token (or use an existing Secret if set to `false`) | `false` |
| `manager.secret.name` | Name of the Secret to be created or reused (will be prefixed with release name when `.create` is `true`) | `hcloud-api-token` |
| `manager.secret.hcloudApiToken` | Hetzner Cloud API token (only required if `.create` is `true`) | `nil` |
| `manager.secret.labels` | Additional labels for Secret resource | `{}` |
| `manager.secret.annotations` | Additional annotations for Secret resource | `{}` |
| `manager.serviceAccount.labels` | Additional labels for ServiceAccount resource | `{}` |
| `manager.serviceAccount.annotations` | Additional annotations for ServiceAccount resource | `{}` |
| `manager.clusterRoleBinding.labels` | Additional labels for ClusterRoleBinding resource | `{}` |
| `manager.clusterRoleBinding.annotations` | Additional annotations for ClusterRoleBinding resource | `{}` |


## License

This project is published under the Apache License, Version 2.0.
See [LICENSE.md](https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/-/blob/master/LICENSE.md) for more information.

Copyright (c) by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
